(ns core
  (:gen-class)
  (:refer-clojure :exclude [+ * /] :rename {- negate})
  (:require [boot.cli :refer [defclifn]]
            [complex.core :refer [complex + - * / abs stringify]]
            [clojure.math.numeric-tower :refer [sqrt]]
            [clojure.string :as str]))

(def func #(- (* % % %) 1))
(def func' #(* 3 (* % %)))

(def root1 (complex 1 0))
(def root2 (complex -0.5 (negate (sqrt 0.75))))
(def root3 (complex -0.5 (sqrt 0.75)))

(def eps 1e-8)

(defn eps-eq? [a b] (> eps (abs (- a b))))
(defn eps-zero? [a] (eps-eq? a 0))
(defn eps-root? [a] (eps-zero? (func a)))

(defn n-iterations [start n]
  (reduce (fn [x _]
            (if (eps-root? x)
              (reduced x)
              (- x (/ (func x) (func' x)))))
          start
          (range n)))

(defn find-root [a b & {i :i}]
  {:pre [(number? a) (number? b)]}
  (n-iterations (complex a b) i))

(defn root->i [root]
  (condp eps-eq? root
    root1 1
    root2 2
    root3 3
    nil))

(defn solve [x y i r]
  (let [root (find-root (Double/parseDouble x) (Double/parseDouble y) :i i)]
    (if r
      (println (stringify root))
      (println (root->i root)))))

(defclifn -main
  [r root bool "Print root value not index"
   i iterations INT int "Number of iterations (default 100)"
   l loop bool "Answer line-by-line queries from stdin"]
  (let [x (nth *args* 0 nil)
        y (nth *args* 1 nil)
        r (:root *opts*)
        i (:iterations *opts* 100)]
    (if (:loop *opts*)
      (doseq [[x y] (partition 2 (str/split (slurp *in*) #"\s+"))]
        (solve x y i r))
      (if (or (nil? x) (nil? y))
        (println "ERROR: you must provide initial value.
                  Usage: [-r/-i VAL/-l] x y.
                  Current values: " *opts* *args*)
        (solve x y i r)))))

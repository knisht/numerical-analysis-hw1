(set-env!
 :source-paths #{"src"}
 :dependencies '[[org.clojure/clojure "1.10.1"]
                 [boot/base "2.8.3"]
                 [boot/core "2.8.3"]

                 [complex "0.1.12"]
                 [org.clojure/math.numeric-tower "0.0.4"]])

(task-options!
 pom {:project 'task3
      :version "0.2"}
 jar {:main 'core}
 aot {:namespace '#{core}})

(deftask build
  "Create a standalone jar file."
  []
  (comp (aot) (uber) (pom) (jar) (target)))

use std::env;

fn run_equation(r: f64, x: f64) -> f64 {
    r * x * (1. - x)
}

fn build_iteration_sequence(r: f64, x0: f64, iterations: u32) -> Vec<f64> {
    let mut current_value = x0;
    let mut values = vec![current_value];
    for _ in 1..iterations {
        current_value = run_equation(r, current_value);
        values.push(current_value)
    }
    values
}

fn evaluate(r: f64, iterations : u32, x0s: &Vec<f64>) -> Vec<f64> {
    x0s.iter()
        .map(|x: &f64|
            *build_iteration_sequence(r, *x, iterations).last().unwrap())
        .collect()
}

fn print_vector<T : ToString>(vec : &Vec<T>) -> () {
    let repr : Vec<String> = vec.iter().map(|x| x.to_string()).collect();
    println!("{}", repr.join(" "))
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let query_type = args[1].as_str();
    match query_type {
        "run_equation" => {
            let r: f64 = args[2].parse().unwrap();
            let x0: f64 = args[3].parse().unwrap();
            println!("{}", run_equation(r, x0))
        }
        "build_iteration_sequence" => {
            let r: f64 = args[2].parse().unwrap();
            let iterations : u32 = args[3].parse().unwrap();
            let x0: f64 = args[4].parse().unwrap();
            print_vector(&build_iteration_sequence(r, x0, iterations));
        }
        "evaluate" => {
            let r : f64 = args[2].parse().unwrap();
            let iterations : u32 = args[3].parse().unwrap();
            let x0s : Vec<f64> = args[4..args.len()].iter().map(|x| x.parse().unwrap()).collect();
            print_vector(&evaluate(r, iterations, x0s.as_ref()))
        }
        _ => { panic!("Unknown query type") }
    }
}

import util.Point;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class SimpleIterations {
    private static final double DELTA = 4.669_201_609_102_990_671_853_203_820_466;
    private static final double R_INF = 3.569_945_671_870_944_901_842_005_151_386;
    private static final double EPS = 0.0001;
    private static final int ITERATIONS = 100_000;

    private static final String BIFURCATION_DIAGRAM_POINTS_FILE = "src/main/bifurcationDiagramPoints";
    private static final String POINCARE_PLOT_POINTS_FILE = "src/main/poincarePlotPoints";
    private static final String SEQUENCE_PLOT_POINTS_FILE = "src/main/sequencePlotPoints";

    private static final double[] R = { 3.0, 3.4494897, 3.5440903, 3.5644073, 3.5687594, 3.5696916, 3.5698913, 3.5699340, R_INF };

    public static double[] iterate(double x, double r, int cycleLen, int iterationsCount) {
        double[] xs = new double[iterationsCount + 1];
        xs[0] = x;
        for (int i = 1; i <= iterationsCount; i++) {
            xs[i] = r * xs[i - 1] * (1 - xs[i - 1]);
        }

        return Arrays.copyOfRange(xs, xs.length - cycleLen, xs.length);
    }

    private static void printPoints(List<Point> points, String file) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(file, true), true)) {
            for (Point point: points) {
                writer.println(point.toString());
            }
        } catch (IOException e) {
            System.err.println(String.format("Error occurred while writing to \"%s\"", file));
        }
    }

    private static void bifurcationDiagramPoints() { // r -> bifurcation limits
        List<Point> points = new ArrayList<>();

        for (int i = 0; i < R.length - 1; i++) {
            int cycleLen = (int) Math.pow(2, i + 1);
            for (double r = R[i]; r < R[i + 1]; r += EPS) {
                final double curR = r;

                double[] lims = iterate(1. / 2., r, cycleLen, ITERATIONS);
                List<Point> newPoints = Arrays.stream(lims)
                        .mapToObj(lim -> new Point(curR, lim))
                        .collect(Collectors.toList());

                points.addAll(newPoints);
            }
        }

        printPoints(points, BIFURCATION_DIAGRAM_POINTS_FILE);
    }

    private static void poincarePlotPoints() { // поиск точек траектории сходимости x^(i) -> rx^(i) * (1 - x^(i))
        double[] rs = { 3.0, 3.5, 3.55, 3.565, 3.569, 3.5697, 3.5699, 3.56994 };

        for (double r : rs) {
            List<Point> points = new ArrayList<>();

            int numberOfBifurcations = getNumberOfBifurcations(r);
            int cycleLen = (int) Math.pow(2, numberOfBifurcations);
            double[] xs = new double[ITERATIONS + 1];
            xs[0] = 1. / 2.;
            int counter = 0;
            int i;
            for (i = 1; i <= ITERATIONS; i++) {
                xs[i] = r * xs[i - 1] * (1 - xs[i - 1]);
                points.add(new Point(xs[i - 1], xs[i]));
                if ((i > cycleLen) && (Math.abs(xs[i] - xs[i - cycleLen]) < EPS)) {
                    if (++counter == cycleLen) {
                        break;
                    }
                } else {
                    counter = 0;
                }
            }
            points.add(new Point(-1, -1));
            printToFile(String.valueOf(r), POINCARE_PLOT_POINTS_FILE);
            printPoints(points, POINCARE_PLOT_POINTS_FILE);
        }
    }

    private static int getNumberOfBifurcations(double r) {
        for (int i = 0; i < R.length; i++) {
            if (r >= R[i] && r < R[i + 1]) {
                return i + 1;
            }
        }
        return -1;
    }

    private static void printToFile(String str, String file) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(file, true), true)) {
            writer.println(str);
        } catch (IOException e) {
            System.err.println(String.format("Error occurred while writing to \"%s\"", file));
        }
    }

    private static void sequencePlotPoints() { // i -> x^(i)
        double[] rs = { 3.0, 3.5, 3.55, 3.565, 3.569, 3.5697, 3.5699, 3.56994 };

        for (double r : rs) {
            List<Point> points = new ArrayList<>();

            double[] xs = new double[100];
            xs[0] = 1. / 2.;
            points.add(new Point(0, xs[0]));
            for (int j = 1; j < 100; j++) {
                xs[j] = r * xs[j - 1] * (1 - xs[j - 1]);
                points.add(new Point(j, xs[j]));
            }
            points.add(new Point(-1, -1));
            printToFile(String.valueOf(r), SEQUENCE_PLOT_POINTS_FILE);
            printPoints(points, SEQUENCE_PLOT_POINTS_FILE);
        }
    }

    public static void main(String[] args) {
        bifurcationDiagramPoints();
        poincarePlotPoints();
        sequencePlotPoints();
    }
}